#ifndef __CONF_H__
#define __CONF_H__

#define MAXFILENAMELEN 256

#define SYSCONFDIR     "/etc/czlc/"	/* default folder */
#define CZLC_CONF_FILE  "czlc.ini"	/* default file */
#define CZLC_GLOBAL_CONF_FILE SYSCONFDIR CZLC_CONF_FILE

struct conf {
	unsigned int jobs;
	unsigned int verbosity;
};


int
conf_ini_handler(void * u, const char * s, const char * f, const char * v);

#endif  /* __CONF_H__ */
