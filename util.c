#include <stdio.h>
#include <stdarg.h>

#include "util.h"

int verbose = 10;

/* debug printf, show info if global verbose >= verbosity */
void
d_printf(uint8_t verbosity, char *fmt, ...) {
	char buffer[MAXLEN];

	if (verbose >= verbosity ) {
		va_list args;
		va_start(args, fmt);
		vsnprintf(buffer, MAXLEN, fmt, args);
		switch(verbosity) {
		case D_INFO:
			fprintf(stderr, "[INFO] %s", buffer);
			break;
		case D_WARN:
			fprintf(stderr, "[WARN] %s", buffer);
			break;
		case D_ERRO:
			fprintf(stderr, "[ERRO] %s", buffer);
			break;
		case D_CRIT:
			fprintf(stderr, "[CRIT] %s", buffer);
			break;
		default:
			fprintf(stderr, "%s\n", buffer);
			break;
		}

		va_end(args);
	}
}
