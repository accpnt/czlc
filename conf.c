/*
 * Reads a configuration file and populates a structure
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "conf.h"
#include "common.h"
#include "util.h"

/*
 * should be used as a callback function for the *.ini parser
 * this will populate the configuration list
 */
int
conf_ini_handler(void * u, const char * s, const char * f, const char * v)
{
	char * end;
	unsigned int rc = SUCCESS;
	struct conf * c;

	if(NULL == u) {
		goto error;
	}
	else {
		c = (struct conf *)u;
	}

	/* TODO : use the u * pointer */

#define MATCH_SECTION(x) (strcmp(s,x) == 0)
#define MATCH_FIELD(x)   (strcmp(f,x) == 0)
#define MATCH_VALUE(x)   (strcmp(v,x) == 0)

	if (MATCH_SECTION("main")) {
		if(MATCH_FIELD("jobs")) {
			c->jobs = atoi(v);
		}
		else if(MATCH_FIELD("verbosity")) {
			c->verbosity = atoi(v);
		}
	}

error:
	return rc;
}
