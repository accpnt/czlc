#ifndef __MATRIX_H__
#define __MATRIX_H__

#include <OpenCL/opencl.h>
#include <lua.h>

typedef struct {
	unsigned int r;
	unsigned int c;
	cl_float * _m;
} matrix_t;

int matrix_print(matrix_t * m);
int matrix_pull(lua_State * L, int idx, matrix_t * m);
int matrix_push(lua_State * L, matrix_t * m);
void matrix_init(matrix_t * m, unsigned int r, unsigned int c);
void matrix_free(matrix_t * m);

#endif  /* __MATRIX_H__ */
