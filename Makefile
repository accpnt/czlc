# Makefile for fb, updated Tue Nov 30 13:49:35 MET 2010
# Make the build silent by default
V = 

ifeq ($(strip $(V)),)
        E = @echo
        Q = @
else
        E = @\#
        Q =
endif
export E Q

TARGET      := czlc
SRCS        := czlc.c conf.c ini.c util.c calc.c matrix.c kernel.c
OBJS        := ${SRCS:.c=.o} 

CC       = gcc
WARNINGS = -Wall -Wstrict-prototypes -pedantic
DEBUG    = -g
INCLUDES = -I /usr/local/include/lua/
CFLAGS   = -O2 $(WARNINGS) $(DEBUG) $(INCLUDES) -Wall -W -Wpointer-arith -Wbad-function-cast
LFLAGS   = -pthread -ldl -llua -framework OpenCL
LIBS     =

.PHONY: all clean distclean 
all:: ${TARGET} ${PLUGIN}

${TARGET}: ${OBJS} 
	$(E) "  LINK    " $@
	$(Q) ${CC} ${LFLAGS} -o $@ $^ ${LIBS} 

${OBJS}: %.o: %.c  
	$(E) "  CC      " $@
	$(Q) ${CC} ${CFLAGS} -o $@ -c $< 

clean:: 
	$(E) "  CLEAN"
	$(Q) -rm -f *~ *.o ${TARGET} ${PLUGIN}

distclean:: clean
