#include <stdio.h>

#include <OpenCL/opencl.h>

#include "kernel.h"
#include "util.h"
#include "common.h"

static cl_context context;         /* compute context */
static cl_kernel kernel;           /* compute kernel */
static cl_command_queue commands;  /* compute command queue */
static char * kernel_source;

/*
 * Given a cl code and return a string represenation
 */
static const char* 
kernel_get_error_string(int errorCode) 
{
	switch (errorCode) {
	case 0: return "CL_SUCCESS";
	case -1: return "CL_DEVICE_NOT_FOUND";
	case -2: return "CL_DEVICE_NOT_AVAILABLE";
	case -3: return "CL_COMPILER_NOT_AVAILABLE";
	case -4: return "CL_MEM_OBJECT_ALLOCATION_FAILURE";
	case -5: return "CL_OUT_OF_RESOURCES";
	case -6: return "CL_OUT_OF_HOST_MEMORY";
	case -7: return "CL_PROFILING_INFO_NOT_AVAILABLE";
	case -8: return "CL_MEM_COPY_OVERLAP";
	case -9: return "CL_IMAGE_FORMAT_MISMATCH";
	case -10: return "CL_IMAGE_FORMAT_NOT_SUPPORTED";
	case -12: return "CL_MAP_FAILURE";
	case -13: return "CL_MISALIGNED_SUB_BUFFER_OFFSET";
	case -14: return "CL_EXEC_STATUS_ERROR_FOR_EVENTS_IN_WAIT_LIST";
	case -15: return "CL_COMPILE_PROGRAM_FAILURE";
	case -16: return "CL_LINKER_NOT_AVAILABLE";
	case -17: return "CL_LINK_PROGRAM_FAILURE";
	case -18: return "CL_DEVICE_PARTITION_FAILED";
	case -19: return "CL_KERNEL_ARG_INFO_NOT_AVAILABLE";
	case -30: return "CL_INVALID_VALUE";
	case -31: return "CL_INVALID_DEVICE_TYPE";
	case -32: return "CL_INVALID_PLATFORM";
	case -33: return "CL_INVALID_DEVICE";
	case -34: return "CL_INVALID_CONTEXT";
	case -35: return "CL_INVALID_QUEUE_PROPERTIES";
	case -36: return "CL_INVALID_COMMAND_QUEUE";
	case -37: return "CL_INVALID_HOST_PTR";
	case -38: return "CL_INVALID_MEM_OBJECT";
	case -39: return "CL_INVALID_IMAGE_FORMAT_DESCRIPTOR";
	case -40: return "CL_INVALID_IMAGE_SIZE";
	case -41: return "CL_INVALID_SAMPLER";
	case -42: return "CL_INVALID_BINARY";
	case -43: return "CL_INVALID_BUILD_OPTIONS";
	case -44: return "CL_INVALID_PROGRAM";
	case -45: return "CL_INVALID_PROGRAM_EXECUTABLE";
	case -46: return "CL_INVALID_KERNEL_NAME";
	case -47: return "CL_INVALID_KERNEL_DEFINITION";
	case -48: return "CL_INVALID_KERNEL";
	case -49: return "CL_INVALID_ARG_INDEX";
	case -50: return "CL_INVALID_ARG_VALUE";
	case -51: return "CL_INVALID_ARG_SIZE";
	case -52: return "CL_INVALID_KERNEL_ARGS";
	case -53: return "CL_INVALID_WORK_DIMENSION";
	case -54: return "CL_INVALID_WORK_GROUP_SIZE";
	case -55: return "CL_INVALID_WORK_ITEM_SIZE";
	case -56: return "CL_INVALID_GLOBAL_OFFSET";
	case -57: return "CL_INVALID_EVENT_WAIT_LIST";
	case -58: return "CL_INVALID_EVENT";
	case -59: return "CL_INVALID_OPERATION";
	case -60: return "CL_INVALID_GL_OBJECT";
	case -61: return "CL_INVALID_BUFFER_SIZE";
	case -62: return "CL_INVALID_MIP_LEVEL";
	case -63: return "CL_INVALID_GLOBAL_WORK_SIZE";
	case -64: return "CL_INVALID_PROPERTY";
	case -65: return "CL_INVALID_IMAGE_DESCRIPTOR";
	case -66: return "CL_INVALID_COMPILER_OPTIONS";
	case -67: return "CL_INVALID_LINKER_OPTIONS";
	case -68: return "CL_INVALID_DEVICE_PARTITION_COUNT";
	case -69: return "CL_INVALID_PIPE_SIZE";
	case -70: return "CL_INVALID_DEVICE_QUEUE";
	case -71: return "CL_INVALID_SPEC_ID";
	case -72: return "CL_MAX_SIZE_RESTRICTION_EXCEEDED";
	case -1002: return "CL_INVALID_D3D10_DEVICE_KHR";
	case -1003: return "CL_INVALID_D3D10_RESOURCE_KHR";
	case -1004: return "CL_D3D10_RESOURCE_ALREADY_ACQUIRED_KHR";
	case -1005: return "CL_D3D10_RESOURCE_NOT_ACQUIRED_KHR";
	case -1006: return "CL_INVALID_D3D11_DEVICE_KHR";
	case -1007: return "CL_INVALID_D3D11_RESOURCE_KHR";
	case -1008: return "CL_D3D11_RESOURCE_ALREADY_ACQUIRED_KHR";
	case -1009: return "CL_D3D11_RESOURCE_NOT_ACQUIRED_KHR";
	case -1010: return "CL_INVALID_DX9_MEDIA_ADAPTER_KHR";
	case -1011: return "CL_INVALID_DX9_MEDIA_SURFACE_KHR";
	case -1012: return "CL_DX9_MEDIA_SURFACE_ALREADY_ACQUIRED_KHR";
	case -1013: return "CL_DX9_MEDIA_SURFACE_NOT_ACQUIRED_KHR";
	case -1093: return "CL_INVALID_EGL_OBJECT_KHR";
	case -1092: return "CL_EGL_RESOURCE_NOT_ACQUIRED_KHR";
	case -1001: return "CL_PLATFORM_NOT_FOUND_KHR";
	case -1057: return "CL_DEVICE_PARTITION_FAILED_EXT";
	case -1058: return "CL_INVALID_PARTITION_COUNT_EXT";
	case -1059: return "CL_INVALID_PARTITION_NAME_EXT";
	case -1094: return "CL_INVALID_ACCELERATOR_INTEL";
	case -1095: return "CL_INVALID_ACCELERATOR_TYPE_INTEL";
	case -1096: return "CL_INVALID_ACCELERATOR_DESCRIPTOR_INTEL";
	case -1097: return "CL_ACCELERATOR_TYPE_NOT_SUPPORTED_INTEL";
	case -1000: return "CL_INVALID_GL_SHAREGROUP_REFERENCE_KHR";
	case -1098: return "CL_INVALID_VA_API_MEDIA_ADAPTER_INTEL";
	case -1099: return "CL_INVALID_VA_API_MEDIA_SURFACE_INTEL";
	case -1100: return "CL_VA_API_MEDIA_SURFACE_ALREADY_ACQUIRED_INTEL";
	case -1101: return "CL_VA_API_MEDIA_SURFACE_NOT_ACQUIRED_INTEL";
	default: return "CL_UNKNOWN_ERROR";
	}
}

/*
 * check cl error, if not CL_SUCCESS, print to stderr
 */
static int 
kernel_check_error(int errorCode) 
{
	int rc = SUCCESS;

	if (errorCode != 0) {
		d_printf(D_ERRO, "%s\n", kernel_get_error_string(errorCode));
		rc = FAILURE;
	}
	return rc;
}

static long 
kernel_load(const char * file, char ** buf)
{
	FILE  *fp;
	size_t fsz = -1L;  /* our return code, assume error by default */
	long   off_end;
	int    rc;

	if((NULL == file) || (NULL == buf)) {
		goto exit;
	}

	/* open the file */
	fp = fopen(file, "r"); 
	if(NULL == fp) {
		goto exit;
	}

	/* seek to the end of the file */
	rc = fseek(fp, 0L, SEEK_END);
	if(0 != rc) {
		goto exit;
	}

	/* byte offset to the end of the file (size) */
	if(0 > (off_end = ftell(fp))) {
		goto exit;
	}
	fsz = (size_t)off_end;

	/* allocate a buffer to hold the whole file */
	*buf = (char *) malloc(fsz + 1);
	if(NULL == *buf) {
		goto exit;
	}

	/* rewind file pointer to start of file */
	rewind(fp);

	/* slurp file into buffer */
	if(fsz != fread(*buf, 1, fsz, fp)) {
		free(*buf);
		fsz = -1;
		goto exit;
	}

	/* close the file */
	if(EOF == fclose(fp)) {
		free(*buf);
		fsz = -1;
		goto exit;
	}

	/* make sure the buffer is NUL-terminated, just in case */
	(*buf)[fsz] = '\0';

	/* return the file size */
exit:
	return (long)fsz;
}

int 
kernel_mult(matrix_t * a, matrix_t * b, matrix_t * c)
{
	int rc = SUCCESS;
	/* OpenCL device memory for matrices */
	cl_mem d_a;
	cl_mem d_b;
	cl_mem d_c;

	cl_event event;
	int err;  /* error code returned from api calls */
	size_t localWorkSize[2], globalWorkSize[2];

	if ((NULL == a) || (NULL == b) || (NULL == c)) {
		d_printf(D_ERRO, "null arguments\n");
		goto exit;
	}

	d_c = clCreateBuffer(context, CL_MEM_READ_WRITE, sizeof(float) * c->r * c->c, NULL, &err);
	d_a = clCreateBuffer(context, CL_MEM_READ_WRITE | CL_MEM_COPY_HOST_PTR, sizeof(float) * a->r * a->c, a->_m, &err);
	d_b = clCreateBuffer(context, CL_MEM_READ_WRITE | CL_MEM_COPY_HOST_PTR, sizeof(float) * b->r * b->c, b->_m, &err);

	err  = clSetKernelArg(kernel, 0, sizeof(cl_mem), &d_c);
	if(err != CL_SUCCESS) {
		rc = kernel_check_error(err);
		goto clean;
	}
	err |= clSetKernelArg(kernel, 1, sizeof(cl_mem), &d_a);
	if(err != CL_SUCCESS) {
		rc = kernel_check_error(err);
		goto clean;
	}
	err |= clSetKernelArg(kernel, 2, sizeof(cl_mem), &d_b);
	if(err != CL_SUCCESS) {
		rc = kernel_check_error(err);
		goto clean;
	}
	err |= clSetKernelArg(kernel, 3, sizeof(int), &a->r);
	if(err != CL_SUCCESS) {
		rc = kernel_check_error(err);
		goto clean;
	}
	err |= clSetKernelArg(kernel, 4, sizeof(int), &c->c);
	if(err != CL_SUCCESS) {
		rc = kernel_check_error(err);
		goto clean;
	}

	localWorkSize[0] = 16;
	localWorkSize[1] = 16;
	globalWorkSize[0] = a->r;
	globalWorkSize[1] = a->c;

	/* enqueue for parallel execution */
	err = clEnqueueNDRangeKernel(commands, kernel, 2, NULL, &globalWorkSize, 0, 0, NULL, &event);
	if(err != CL_SUCCESS) {
		rc = kernel_check_error(err);
		goto clean;
	}

	err = clEnqueueReadBuffer(commands, d_c, CL_TRUE, 0, sizeof(float) * c->r * c->c, c->_m, 0, NULL, NULL);
	if(err != CL_SUCCESS) {
		rc = kernel_check_error(err);
		goto clean;
	}

clean:
	clReleaseMemObject(d_a);
	clReleaseMemObject(d_b);
	clReleaseMemObject(d_c);
exit:
	return rc;
}

int 
kernel_init(void) 
{
	int rc = SUCCESS;
	long kernel_size;

	int err;                    /* error code returned from api calls */
	cl_device_id device_id;     /* compute device id */
	cl_program program;         /* compute program */

	cl_uint dev_cnt = 0;
	clGetPlatformIDs(0, 0, &dev_cnt);
	cl_platform_id platform_ids[100];
	err = clGetDeviceIDs(platform_ids[0], CL_DEVICE_TYPE_DEFAULT, 1, &device_id, NULL);
	if(err != CL_SUCCESS) {
		rc = kernel_check_error(err);
		goto exit;
	}
	context = clCreateContext(0, 1, &device_id, NULL, NULL, &err);
	if(err != CL_SUCCESS) {
		rc = kernel_check_error(err);
		goto exit;
	}
	commands = clCreateCommandQueue(context, device_id, CL_QUEUE_PROFILING_ENABLE, &err);
	if(err != CL_SUCCESS) {
		rc = kernel_check_error(err);
		goto exit;
	}

	/* create the compute program from the source file */
	kernel_size = kernel_load("kernel/matrix_mult.cl", &kernel_source);
	if(err != CL_SUCCESS) {
		rc = kernel_check_error(err);
		goto exit;
	}

	program = clCreateProgramWithSource(context, 1, (const char **) &kernel_source, NULL, &err);
	if(err != CL_SUCCESS) {
		rc = kernel_check_error(err);
		goto exit;
	}
	err = clBuildProgram(program, 0, NULL, NULL, NULL, NULL);
	if(err != CL_SUCCESS) {
		rc = kernel_check_error(err);
		goto exit;
	}
	kernel = clCreateKernel(program, "matrix_mult", &err);
	if(err != CL_SUCCESS) {
		rc = kernel_check_error(err);
		goto exit;
	}

exit:
	return rc;
}

void 
kernel_free(void)
{
	if (kernel_source) {
		free(kernel_source);
	}
}
