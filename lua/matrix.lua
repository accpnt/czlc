--avg, sum = average(10, 20, 30, 40, 50, 60)

--print("The average is ", avg)
--print("The sum is ", sum)


function mp(M)
	for i = 1, #M do
		for j = 1, #M[i] do
			local s = tostring(M[i][j])
			io.write(s, " ")
		end
		io.write('\n')
	end
end

n = 7
m = 5

-- create matrix
A = {}
B = {}
for i = 1, n do
	A[i] = {}
	B[i] = {}
	for j = 1, m do
		A[i][j] = i
		B[i][j] = i + j
	end
end

C = mm(A,B)
mp(C)
