#ifndef __CALC_H__
#define __CALC_H__

#include "conf.h"

int calc_init(struct conf * c);
void calc_free(void);

#endif  /* __CALC_H__ */
