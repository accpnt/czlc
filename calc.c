/*
 * calculus module 
 */
#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>
#include <unistd.h>
#include <errno.h>
#include <assert.h>

#include <lua.h>
#include <lualib.h>
#include <lauxlib.h>

#include "calc.h"
#include "common.h"
#include "util.h"
#include "matrix.h"
#include "kernel.h"

extern struct conf c;
static pthread_t calc;
static lua_State * L;  /* the Lua interpreter */

static int 
average(lua_State * L)
{
	int n = lua_gettop(L);  /* get number of arguments */
	double sum = 0;
	int i;

	for (i = 1; i <= n; i++) {  /* loop through each argument */
		if (!lua_isnumber(L, i)) {
			lua_pushstring(L, "Incorrect argument to 'average'");
			lua_error(L);
		}

		sum += lua_tonumber(L, i);  /* total the arguments */
	}

	lua_pushnumber(L, sum / n);  /* push the average */
	lua_pushnumber(L, sum);      /* push the sum */

	return 2;  /* return the number of results */
}

/* matrix multiplication */
static int
mm(lua_State * L)
{
	matrix_t a;
	matrix_t b;
	matrix_t c;

	/* get number of arguments */
	int n = lua_gettop(L);
	if(2 == n) {
		matrix_pull(L, 1, &a);
		matrix_pull(L, 2, &b);

		c.r = a.r;
		c.c = b.c;
		matrix_init(&c, a.r, b.c);

		kernel_mult(&a, &b, &c);

		matrix_push(L, &c);
	}

	d_printf(D_INFO, "freeing memory\n");
	matrix_free(&a);
	matrix_free(&b);
	matrix_free(&c);

	return 1;
}

/* 
 * This function will initialize Lua and the thread
 */
int 
calc_init(struct conf * c)
{
	int status;
	int rc = FAILURE;  /* assuming failure by default */

	if(NULL == c) {
		d_printf(D_ERRO, "null configuration\n");
		goto exit;
	}

	kernel_init();

	/* initialize Lua */
	L = luaL_newstate();
	luaL_openlibs(L);
	lua_register(L, "average", average);
	lua_register(L, "mm", mm);

	/* load the file containing the script we are going to run */
	status = luaL_loadfile(L, "lua/matrix.lua");
	if (status) {
		d_printf(D_CRIT, "error loading file\n");
		goto exit;
	}

	status = lua_pcall(L, 0, 1, 0);
	if (status != LUA_OK ) {
		d_printf(D_CRIT, "error running file\n");
		goto exit;
	}

	rc = SUCCESS;  /* no errors */
exit:
	d_printf(D_INFO, "%s: exiting (rc = %d)\n", __func__, rc);
	return rc;
}  /* calc_init */

void 
calc_free(void)
{
	lua_close(L);
	kernel_free();
}
