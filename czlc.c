#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <pthread.h>
#include <unistd.h>
#include <signal.h>

#include "common.h"
#include "calc.h"
#include "conf.h"
#include "util.h"
#include "ini.h"        /* parser for *.ini configuration file */

/*----------------------------------------------------------------------------*/
struct conf c;

/*----------------------------------------------------------------------------*/
static void
usage(void)
{
	fprintf(stderr, "%s %d.%d - %s\n", NAME, VERSION, REVISION, DESC);
	fprintf(stderr, "usage: %s [-v] [-c <ini_file>] [-j <jobs>]\n", NAME);
}

static void *
sighandler(void *arg)
{
	int       sig;
	sigset_t  signal_set;

	for (;;) {
		sigfillset(&signal_set);
		sigwait(&signal_set, &sig);

		switch(sig) { /* if we get this far, we've caught a signal */
		case SIGINT:
		case SIGTERM:
			d_printf(D_WARN, "caught SIGINT/SIGTERM, exiting...\n");
			pthread_exit(NULL);
		case SIGSEGV:
			d_printf(D_WARN, "SIGSEGV !!\n");
			break;
		default:
			break;
		}
	}
}

int main(int argc, char * argv[])
{
	int i;
	unsigned int cflag = SUCCESS;  /* assume invalid config by default */
	unsigned int rc = FAILURE;     /* assume error by default */
	sigset_t   signal_set;
	pthread_t  sig_thread;

	/* argument check */
	if(argc == 2 && argv[1][0] == '-' && argv[1][1] == 'h') {
		usage();
		return 0;
	}

	/* parse command line options */
	for(i = 1; (i + 1 < argc) && (argv[i][0] == '-'); i++) {
		switch (argv[i][1]) {
		case 'v':           /* verbose */
			verbose = atoi(argv[++i]);
			break;
		case 'c':           /* read *.ini configuration file */
			d_printf(D_INFO, "parsing '%s'... \n", argv[++i]);

			if(ini_parse(argv[i], conf_ini_handler, &c) < 0) {
				fprintf(stderr, "ERROR\n");
				cflag = FAILURE;
			} else {
				cflag = SUCCESS;
			}
			break;
		case 'j':
			break;
		default:            /* something's wrong */
			d_printf(D_WARN, "unknown configuration switch\n");
			usage();
			break;
		}
	}

	if (SUCCESS == cflag) {   /* config file parsed successfully */
		if (calc_init(&c) < 0)
			goto calc_failed;

		d_printf(D_CRIT, "%s started successfully\n", NAME);

		/* create the signal handling thread */
		if (pthread_create(&sig_thread, NULL, sighandler, NULL))
			goto sighandler_failed;

		/* waiting for the signal handling thread to terminate */
		pthread_join(sig_thread, NULL);

		rc = 0;  /* no error */
	}

sighandler_failed:
calc_failed:
	calc_free();
exit:
	d_printf(D_CRIT, "%s: exiting\n", __func__);
	return 0;
}
