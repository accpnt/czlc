/* 
 * matrix_mult.cl 
 * Matrix multiplication: C = A * B.
 * Device code.
 */
 
/* OpenCL Kernel */
__kernel void
matrix_mult(__global float* C, __global float* A, __global float* B, int wA, int wB)
{
	int tx = get_global_id(0); 
	int ty = get_global_id(1);

	/* value stores the element that is computed by the thread */
	float value = 0;
	for (int k = 0; k < wA; ++k)
	{
		float element_a = A[ty * wA + k];
		float element_b = B[k * wB + tx];
		value += element_a * element_b;
	}

	/* write the matrix to device memory, each thread writes one element */
	C[ty * wA + tx] = value;
}