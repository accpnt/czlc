#ifndef __KERNEL_H__
#define __KERNEL_H__

#include "matrix.h"

int kernel_init(void);
int kernel_mult(matrix_t * a, matrix_t * b, matrix_t * c);
void kernel_free(void);

#endif  /* __KERNEL_H__ */
