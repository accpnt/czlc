#ifndef __COMMON_H__
#define __COMMON_H__

#define NAME      ("czlc")
#define VERSION   (0)
#define REVISION  (1)
#define DESC      ("Calculus")

#define TRUE   (1)
#define FALSE  (0)

/* generic return values */
#define SUCCESS  (0)
#define FAILURE  (-1)

#endif  /* __COMMON_H__ */
