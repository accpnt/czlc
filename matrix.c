#include <stdio.h>
#include <stdlib.h>

#include <lua.h>

#include "matrix.h"
#include "common.h"
#include "util.h"

int 
matrix_print(matrix_t * m)
{
	unsigned int i = 0;
	unsigned int j = 0;
	int rc = SUCCESS;  /* assume success by default */

	if (NULL == m) {
		rc = FAILURE;
		goto exit;
	}

	printf("\n");

	for (i = 0; i < m->r; i++) {
		for (j = 0; i < m->c; j++) {
			printf("%f ", m->_m[i * m->c + j]);
		}
	}

	printf("\n");

exit:
	return rc;
}

int 
matrix_pull(lua_State * L, int idx, matrix_t * m)
{
	int i = 0;
	int j = 0;
	int is_allocated = FALSE;
	int rc = SUCCESS;  /* assume success by default */

	if (NULL == m) {
		rc = FAILURE;
		goto exit;
	}

	if(lua_istable(L, idx)) {
		m->r = lua_rawlen(L, idx);  /* retrieve number of rows */

		lua_pushnil(L);  /* first key: iterating the first dimension */
		while (lua_next(L, idx) != 0) {
			m->c = lua_rawlen(L, -1);  /* retrieve number of columns */

			if(FALSE == is_allocated) {
				m->_m = malloc(m->r * m->c * sizeof(float));
				if(NULL == m->_m) {
					d_printf(D_ERRO, "malloc error\n");
					rc = FAILURE;
					goto exit;
				}
				else {
					is_allocated = TRUE;
				}
			}

			if(lua_istable(L, -1)) {
				lua_pushnil(L);  /* first key: iterating the second dimension */
				j = 0;  /* reset row counter */
				while (lua_next(L, -2) != 0) { /* table is in the stack at index -2 */
					m->_m[i * m->c + j] = lua_tonumber(L, -1);  /* value is at index -1 */
					lua_pop(L, 1);  /* removes 'value'; keeps 'key' for next iteration */
					j++;
				}
			}
			lua_pop(L, 1);  /* removes 'value'; keeps 'key' for next iteration */
			i++;
		}
	}

exit:
	return rc;
}

int 
matrix_push(lua_State * L, matrix_t * m)
{
	unsigned int i = 0;
	unsigned int j = 0;

	lua_newtable(L);
	for(i = 0; i < m->r; i++) {
		lua_pushnumber(L, i + 1);    // parent table index
		lua_newtable(L);             // child table
		for(j = 0; j < m->c; j++ ) {
			lua_pushnumber( L, j + 1 );  // this will be the child's index
			lua_pushnumber( L, m->_m[i * m->c + j]);  // this will be the child's value
			lua_settable( L, -3 );
		}
		lua_settable( L, -3 );
	}

	printf("\n");

	return 1;
}

void
matrix_init(matrix_t * m, unsigned int r, unsigned int c)
{
	if(m) {
		m->r = r;
		m->c = c;
		m->_m = malloc(sizeof(float) * r * c);
	}
}

void 
matrix_free(matrix_t * m)
{
	if(m && m->_m) {
		free(m->_m);
	}
}
